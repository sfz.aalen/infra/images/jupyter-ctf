FROM docker.io/jupyter/minimal-notebook:notebook-6.5.4
RUN rm -d /home/jovyan/work
COPY src/* /home/jovyan/
RUN pip install jupyter_server_mathjax